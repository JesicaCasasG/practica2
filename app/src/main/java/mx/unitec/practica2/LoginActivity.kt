package mx.unitec.practica2

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.EditText
import android.widget.Toast

class LoginActivity : AppCompatActivity() {

    private lateinit var editTextTextEmailAddress2: EditText
    private lateinit var editTextTextPassword2: EditText
    private lateinit var button: Button

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)

        editTextTextEmailAddress2 = findViewById(R.id.editTextTextEmailAddress2)
        editTextTextPassword2 = findViewById(R.id.editTextTextPassword2)
        button = findViewById(R.id.button)

        button.setOnClickListener {

            if(editTextTextEmailAddress2.text.isEmpty()) {

                editTextTextEmailAddress2.error = getString(R.string.error_text)
                return@setOnClickListener
            }

            if(editTextTextPassword2.text.isEmpty()){
                editTextTextPassword2.error = "No puede ser vacío"
                return@setOnClickListener
            }




            Toast.makeText(this,
                "${getString(R.string.welcome_message)} ${editTextTextEmailAddress2.text}",
                Toast.LENGTH_SHORT).show()
        }
    }
}